package com.nannapat.lab11;

public class Submarine extends Vechicle {

    public Submarine(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Submarine (" + this.getName() + ")";
    }
}

