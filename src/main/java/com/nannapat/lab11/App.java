package com.nannapat.lab11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();

        Plane boeing = new Plane("Boeing", "Rosario");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();

        Superman clark = new Superman("Clark");
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.swim();
        
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();

        Submarine boat = new Submarine("Boat", "Kuznetsov");
        System.out.println(boat);

        Bus bus = new Bus("Bus", "Bus");
        System.out.println(bus);

        Bat bat = new Bat("Batty");
        bat.eat();
        bat.sleep();
        bat.walk();
        bat.run();
        bat.takeoff();
        bat.fly();
        bat.landing();

        Rat rat = new Rat("Mickey");
        rat.walk();
        rat.run();
        rat.eat();
        rat.sleep();

        Dog dog = new Dog("Odie");
        dog.walk();
        dog.run();
        dog.eat();
        dog.sleep();

        Cat cat = new Cat("Garfield");
        cat.walk();
        cat.run();
        cat.eat();
        cat.sleep();

        Fish fish = new Fish("Nemo");
        fish.swim();
        fish.eat();
        fish.sleep();

        Snake snake = new Snake("Codciew");
        snake.craw();
        snake.eat();
        snake.sleep();

        Crocodile croc = new Crocodile("Tuamtiam");
        croc.swim();
        croc.walk();
        croc.run();
        croc.eat();
        croc.sleep();
        croc.craw();

        Flyable[] flyables = {clark, boeing, bird1, bat};
        for(int i=0; i<flyables.length;i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        Walkable[] walkables = {clark, cat, bird1, bat, dog, man1, rat};
        for(int i=0; i<walkables.length;i++){
            walkables[i].walk();
            walkables[i].run();
        }
        Swimable[] swimables = {clark, fish, man1, croc};
        for(int i=0; i<swimables.length;i++){
            swimables[i].swim();
        }
        Crawable[] crawables = {snake, croc};
        for(int i=0; i<crawables.length;i++){
            crawables[i].craw();
        }
    }
}
