package com.nannapat.lab11;

public interface Flyable {
    public void takeoff();
    public void fly();
    public void landing();
}
