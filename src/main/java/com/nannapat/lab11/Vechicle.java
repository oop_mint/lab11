package com.nannapat.lab11;

public abstract class Vechicle {
    private String name;
    private String engine;

    public Vechicle(String name,String engine){
        this.name = name;
        this.engine = engine;
    }
    public String getName(){
        return this.name;
    }
    public String getEngine(){
        return this.engine;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setEngine(String engine){
        this.engine = engine;
    }
}
